
// APPROACH - 1

const readLine = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout,
})

const code = []

console.log("Enter JavaScript code snippet (type 'run' to submit / 'exit' for Exit):");

readLine.prompt();

readLine.on('line', (line) => {
    if (line.trim() === 'exit') {
        readLine.close();
    } else {
        const codez = line
        sendToServer(codez)   
    }
})

readLine.on('close', () => {
    console.log('program closed.');
})


async function sendToServer(codez) {
    try {
        const response = await fetch('http://localhost:3000/send-code', {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({ codez }),
        })
        
        if (response.status === 200) {
            const resp = await response.json()
            console.log(resp.message)
        } else if (response.status === 500) {
            const resp = await response.json()
            console.error(resp.error)
        } else {
            console.log(error)
        }
    } catch (error) {
        console.log(error);
    }
}

// APPROACH - 2

// const readLine = require('readline').createInterface({
//     input: process.stdin,
//     output: process.stdout,
// })

// const code = []

// console.log("Enter JavaScript code snippet (type 'run' to submit / 'exit' for Exit):");
// readLine.on('line', (line) => {
//     if (line.trim() === 'run') {
//         const codez = code.join('\n')
//         sendToServer(codez)
//         code.length = 0
//     } else if (line.trim() === 'exit') {
//         readLine.close();
//     } else {
//         code.push(line);
//     }
// })
// readLine.prompt();

// async function sendToServer(codez) {
//     try {
//         const response = await fetch('http://localhost:3000/send-code', {
//             method: 'POST',
//             headers: {
//                 "Content-Type": "application/json",
//             },
//             body: JSON.stringify({ codez }),
//         })
        
//         if (response.status === 200) {
//             const resp = await response.json()
//             console.log(resp.message)
//         } else if (response.status === 500) {
//             const resp = await response.json()
//             console.error(resp.error)
//         } else {
//             console.log(error)
//         }
//     } catch (error) {
//         console.log(error);
//     }
// }
