const express = require('express')
const app1 = express()

app1.use(express.json())

app1.post('/send-code', async (req, res) => {
    const receiveData = req.body.codez;
    let result = receiveData
    try {
        const body = { result }
        const response = await fetch('http://localhost:8080/execute-code', {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(body)
        });
        
        if (response.status === 200) {
            const resp = await response.json();
            console.log(resp.message)
            res.status(200).json({message: "data recieved {from server 1}"})
        } else if (response.status === 500) {
            const resp = await response.json();
            console.error(resp.error);
            res.status(500).json({error: resp.error})
        }
    } catch (error) {
        console.log("err: ", error);
    }
})

const port = 3000
app1.listen(port, () => {
    console.log(`server running on port ${port}`);
})