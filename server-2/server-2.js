const express = require('express')

const app2 = express()

app2.use(express.json())

app2.post('/execute-code', async (req, res) => {
    const receiveData = req.body.result;
    try {
        const codeToExecute = eval(receiveData);
        res.status(200).json({message: 'data recieved {from server 2}'})
    } catch (error) {
        console.error('Error executing code:', error.message);
        res.status(500).json({error: error.message});
    }
})

const port = 8080
app2.listen(port, () => {
    console.log(`server running on port ${port}`);
})